window.onload= function(){
	//console.log("js is linked")
	getReimbursementList();
}

function getReimbursementList() {
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState == 4 && xhttp.status ==200){
			let reimList  = JSON.parse(xhttp.responseText);
			//console.log(reimList );
			for (let reims of reimList) {
				//console.log(reims);
				reimburseLists(reims);
			}
		}
	}
	xhttp.open("GET", "http://localhost:7002/reimbursement/listbyuser");
	xhttp.send();
}


function reimburseLists(reims) {

	let status = "";
	let type = "";

	switch (reims.statusId) {
		case 1:
			status = "Pending";
			break;

		case 2:
			status = "Declined";
			break;

		case 3:
			status = "Approved";
			break;

		default:
			break;

	}

	switch (reims.typeId) {

		case 1:
			type = "Lodging";
			break;

		case 2:
			type = "Travel";
			break;

		case 3:
			type = "Food";
			break;

		case 4:
			type = "Other";
			break;

		default:
			break;


	}
	//var submitTime = new Date(reims.submittedDate);
	var submitTime = getFormattedDate(reims.submittedDate);
	var resolved = getFormattedDate(reims.resolvedDate);
	//var resolved = (reims.resolvedDate) ? reims.resolvedDate : '';
	
	var info = $(`
				<tr>
					<th>${reims.id}</th>
					<td>$${reims.amount}</td>
					<td>${submitTime}</td>
					<td>${resolved}</td>
					<td>${reims.description}</td>
					<td>${reims.author}</td>
					<td>${status}</td>
					<td>${type}</td>
				</tr>`
	)

	$('#reimInfo').append(info);

}



function getFormattedDate(dateIn) {
	if (dateIn == null) {
		return "";
	};
	let date = new Date(dateIn);
    let month = date.getMonth() + 1;
    let day = date.getDate();
    let hour = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();
    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;
    let str = date.getFullYear() + "-" + month + "-" + day + " " +  hour + ":" + min + ":" + sec;
    return str;
};



function status() {

	let input, filter, table, tr, td, i, txtValue;
	input = document.getElementById('select');
	filter = input.value.toLowerCase();
	table = document.getElementById('tablelist');
	tr = table.getElementsByTagName("tr");

	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[6];
		if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toLowerCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			}
			else {
				tr[i].style.display = "none";
			}

		}

	}
}


