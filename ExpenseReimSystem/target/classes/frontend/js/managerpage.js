window.onload = function() {
	//console.log("js is linked")
	getReimbursementList();
}

function getReimbursementList() {
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let reimList = JSON.parse(xhttp.responseText);
			//console.log(reimList );
			for (let reims of reimList) {
				//console.log(reims);
				reimburseLists(reims);
			}
		}
	}
	xhttp.open("GET", "http://localhost:7002/reimbursement/list");
	xhttp.send();
}

function reimburseLists(reims) {

	let status = "";
	let type = "";

	switch (reims.statusId) {
		case 1:
			status = "Pending";
			break;

		case 2:
			status = "Declined";
			break;

		case 3:
			status = "Approved";
			break;

		default:
			break;

	}

	switch (reims.typeId) {

		case 1:
			type = "Lodging";
			break;

		case 2:
			type = "Travel";
			break;

		case 3:
			type = "Food";
			break;

		case 4:
			type = "Other";
			break;

		default:
			break;


	}
	//var submitTime = new Date(reims.submittedDate);
	//var resolvedDate = new Date(reims.resolvedDate)
	//var resolved = (resolvedDate) ? resolvedDate : 'Not approved';
	var submitTime = getFormattedDate(reims.submittedDate);
	var resolved = getFormattedDate(reims.resolvedDate);


	var info = $(`
				<tr>
					<th>${reims.id}</th>
					<td>$${reims.amount}</td>
					<td>${submitTime}</td>
					<td>${resolved}</td>
					<td>${reims.description}</td>
					<td>${reims.author}</td>
					<td>${reims.resolver}</td>
					<td id = "status${reims.id}">${status}</td>
					<td>${type}</td>
					<td>
						<a href="managerPageReimList.html" class="button1" onclick = "updateStatus(${reims.id}, 3)">Approved</a>
						<a href="managerPageReimList.html" class = "button2" onclick = "updateStatus(${reims.id}, 2)")">Declined</a>
					</td>
				</tr>`
	)

	$('#reimInfo').append(info);
}


function updateStatus(r_id, status) {

	var stat = {

		id: r_id,
		status_id: status

	}

	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {

		if (xhr.readyState == 4 && xhr.status == 200) {

			$(`status${r_id}`).html(status);
			refreshStatus();

		}

	}

	xhr.open("PUT", "/reimbursement/update", true);
	xhr.setRequestHeader("Content-type", "application/json");
	var toSend = JSON.stringify(stat);
	xhr.send(toSend);

}


function getFormattedDate(dateIn) {
	if (dateIn == null) {
		return "";
	};
	let date = new Date(dateIn);
	let month = date.getMonth() + 1;
	let day = date.getDate();
	let hour = date.getHours();
	let min = date.getMinutes();
	let sec = date.getSeconds();
	month = (month < 10 ? "0" : "") + month;
	day = (day < 10 ? "0" : "") + day;
	hour = (hour < 10 ? "0" : "") + hour;
	min = (min < 10 ? "0" : "") + min;
	sec = (sec < 10 ? "0" : "") + sec;
	let str = date.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
	return str;
};


function status() {

	let input, filter, table, tr, td, i, txtValue;
	input = document.getElementById('selectBy');
	filter = input.value.toLowerCase();
	table = document.getElementById('customers');
	tr = table.getElementsByTagName("tr");

	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[6];
		if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toLowerCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			}
			else {
				tr[i].style.display = "none";
			}

		}

	}
}




