
window.onload= function(){
	console.log("js is linked")
	getSessionUser();
}

function getSessionUser() {
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState == 4 && xhttp.status ==200){
			let user = JSON.parse(xhttp.responseText);
			console.log(user);
			if(user) {
				document.getElementById('login_form').style.display = 'none';
				document.getElementById('success_login').style.display = 'block';
			}
		}
	}
	
	xhttp.open("GET", "http://localhost:7002/login/session");
	xhttp.send();
}