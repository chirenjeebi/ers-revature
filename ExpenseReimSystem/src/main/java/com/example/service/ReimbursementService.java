package com.example.service;


import java.util.List;

import com.example.dao.EmployeeDao;
import com.example.dao.ReimbursementDao;
import com.example.model.Employee;
import com.example.model.Reimbursement;


public class ReimbursementService {
	
	private ReimbursementDao reDao;
	
	public ReimbursementService() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementService(ReimbursementDao reDao) {
		super();
		this.reDao = reDao;
	}
	
	public void createReimbRequest(Reimbursement reim) {
		reDao.insertRequest(reim);	
	}
	
	public List<Reimbursement> getReimburmentList(){
		List<Reimbursement> reim = reDao.selectRequestAll();
		return reim;
	}
	
	public List<Reimbursement> getReimbersementListByUserId(int author){
		List<Reimbursement> reim = reDao.getByEmployeeId(author);
		return reim;
	}
	
	// ...............................
	public void updateReimbursement(Reimbursement reimId) {
		reDao.updateReimbursement(reimId);	
	}
	
	public Reimbursement deleteReimbursementById(int reimId) {
		return reDao.deleteById(reimId);
	}
	
	public List<Reimbursement> selectReimbursementByStatus(int statusTypeId){
		List<Reimbursement> reim = reDao.SelectByStatusType(statusTypeId);
		return reim;
	}
	
}
