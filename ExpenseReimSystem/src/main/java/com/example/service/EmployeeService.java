package com.example.service;

import com.example.dao.EmployeeDao;
import com.example.model.Employee;

public class EmployeeService {
	
	private EmployeeDao empDao;
	
	 public EmployeeService() {
		// TODO Auto-generated constructor stub
	}
	 
	
	public EmployeeService(EmployeeDao empDao) {
		super();
		this.empDao = empDao;
	}


	public void createEmployeeUser(Employee emp) {
		
		empDao.insert(emp);
	}
	
	public Employee getUser(String name) {

		Employee user = empDao.selectByUser(name);
		if(user == null) {
			throw new NullPointerException();
		}
		return user;
	}
	
	public boolean userVerify(String name, String password) {
		boolean isVerified = false;
		Employee user = getUser(name);
		if(user.getPassword().equals(password)) {
			isVerified = true;
		}
		return isVerified;
	}

}
