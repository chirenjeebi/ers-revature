package com.example.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	public DBConnection() {};
	private static Connection con;
	private static String url = "jdbc:mariadb://database-1.crm2rxoxsody.us-east-1.rds.amazonaws.com:3306/projectdb";
	private static String username = "projectuser";
	private static String password = "mypassword";

	public  Connection getMyConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);

	}
}
