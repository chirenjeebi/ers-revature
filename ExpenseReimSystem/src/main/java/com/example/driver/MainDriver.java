package com.example.driver;

import org.apache.log4j.Logger;

import com.example.connection.DBConnection;
import com.example.controller.EmployeeController;
import com.example.controller.ReimbursementController;
import com.example.dao.EmployeeDao;
import com.example.dao.ReimbursementDao;
import com.example.service.EmployeeService;
import com.example.service.ReimbursementService;

import io.javalin.Javalin;

public class MainDriver {

	public final static Logger log =Logger.getLogger(MainDriver.class);
	public static void main(String[] args) {
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/frontend");
		});
		EmployeeController eCon = new EmployeeController(new EmployeeService(new EmployeeDao(new DBConnection())));
		ReimbursementController reCon = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new DBConnection())));
		app.start(7002);
		
		app.get("/", eCon.GET_HOME);
		
		app.post("/employee/register", eCon.POST_USER);
		
		app.post("/user/login", eCon.POST_LOGIN);
		
		app.get("/user/logout", eCon.POST_LOGOUT);
		
		app.get("/login/session", eCon.getSessUser);
		
		app.post("/reimbursement/request", reCon.POST_REQUEST);
		
		app.get("/reimbursement/listbyuser", reCon.Get_ReimbersementListByUserId);
		
		app.get("/reimbursement/list", reCon.GET_REIMBURSEMENTLIST);
		
		app.delete("/reimbursement/delete", reCon.Delete_ReimbursementById);
		
		app.put("/reimbursement/update", reCon.Put_updateReimbursement);
		
		app.exception(NullPointerException.class, (c, ctx) -> {
			ctx.status(404);
			ctx.result("User Does Not Exits");
		});
	}

}
