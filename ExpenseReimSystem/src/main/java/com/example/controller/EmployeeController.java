package com.example.controller;

import com.example.driver.MainDriver;
import com.example.model.Employee;
import com.example.model.UserRoles;
import com.example.service.EmployeeService;

import io.javalin.http.Handler;

public class EmployeeController {
	
	 

	EmployeeService empService;
	
	public EmployeeController() {
		// TODO Auto-generated constructor stub
	}

	public EmployeeController(EmployeeService empService) {
		super();
		this.empService = empService;
	}
	
	public  final Handler GET_HOME  = (ctx) -> {
		ctx.redirect("/html/home.html");
	};

	public  final Handler POST_LOGOUT  = (ctx) -> {
		ctx.sessionAttribute("user", null);
		ctx.redirect("/html/login.html");
	};
	
	public Handler POST_USER = (ctx) -> {

		String username = ctx.formParam("username");
		String password = ctx.formParam("password");
		String fName = ctx.formParam("firstName");
		String lName = ctx.formParam("lastName");
		String email = ctx.formParam("email");
		int userRole = Integer.parseInt(ctx.formParam("roletype"));
		empService.createEmployeeUser(new Employee(fName, lName, username, password, email, userRole));	
		ctx.sessionAttribute("user", null);
		ctx.redirect("/html/login.html");
	};
	
	public Handler POST_LOGIN = (ctx) -> {		
		if(empService.userVerify(ctx.formParam("username"), ctx.formParam("password"))){
			MainDriver.log.info("successful login" + ctx.formParam("username"));
			ctx.sessionAttribute("user", empService.getUser(ctx.formParam("username")));
			Employee emp = empService.getUser(ctx.formParam("username"));
			if(emp.getUserRole()==1) {
				ctx.redirect("/html/manager.html");
			}else if(emp.getUserRole()==2) {
				ctx.redirect("/html/user.html");
			}else {
				ctx.redirect("/html/404.html");
			}
		}
	};
	
	public  Handler getSessUser = (ctx) -> {
		Employee user = (Employee) ctx.sessionAttribute("user");
		if(user != null)
			ctx.json(user);
	};
};


