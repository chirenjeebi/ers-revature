package com.example.model;

import java.time.LocalDateTime;

public class UserRecovery {
	private int id;
	private String token;
	private LocalDateTime creationTime;
	private Employee requester;
	
	public UserRecovery() {
		// TODO Auto-generated constructor stub
	}

	public UserRecovery(int id, String token, LocalDateTime creationTime, Employee requester) {
		super();
		this.id = id;
		this.token = token;
		this.creationTime = creationTime;
		this.requester = requester;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public Employee getRequester() {
		return requester;
	}

	public void setRequester(Employee requester) {
		this.requester = requester;
	}

	@Override
	public String toString() {
		return "UserRecovery [id=" + id + ", token=" + token + ", creationTime=" + creationTime + ", requester="
				+ requester + "]";
	}
}
