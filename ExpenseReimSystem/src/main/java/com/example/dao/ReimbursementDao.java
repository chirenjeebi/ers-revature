package com.example.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.connection.DBConnection;
import com.example.model.Reimbursement;

public class ReimbursementDao implements ReimGenericDao<Reimbursement> {
	private DBConnection dbcon;

	public ReimbursementDao() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementDao(DBConnection dbcon) {
		super();
		this.dbcon = dbcon;
	}

	@Override
	public boolean insertRequest(Reimbursement reimburse) {
		boolean request = false;
		PreparedStatement prepStmt;
		try (Connection con = dbcon.getMyConnection()) {
			prepStmt = con.prepareStatement("insert into ers_reimbursement"
					+ "(reimb_amount, reimb_submitted, reimb_resolved, reimb_description, reimb_author, reimb_resolver, reimb_status_id, reimb_type_id)"
					+ " values (?, ?, ?, ?, ?, ?, ?, ?)");
			prepStmt.setDouble(1, reimburse.getAmount());
			prepStmt.setTimestamp(2, reimburse.getSubmittedDate());
			prepStmt.setDate(3, (Date) reimburse.getResolvedDate());
			prepStmt.setString(4, reimburse.getDescription());
			prepStmt.setInt(5, reimburse.getAuthor());
			prepStmt.setInt(6, reimburse.getResolver());
			prepStmt.setInt(7, reimburse.getStatusId());
			prepStmt.setInt(8, reimburse.getTypeId());
			request = prepStmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return request;
	}

	// this will return all the reimbursement request list to the manager.
	@Override
	public List<Reimbursement> selectRequestAll() {
		List<Reimbursement> reims = new ArrayList<Reimbursement>();
		PreparedStatement prepStmt;
		try (Connection con = dbcon.getMyConnection()) {
			String sql = "SELECT * FROM ers_reimbursement ORDER BY reimb_id";
			prepStmt = con.prepareStatement(sql);
			ResultSet rs = prepStmt.executeQuery();

			while (rs.next()) {
				Reimbursement reimbursement = new Reimbursement();
				reimbursement.setId(rs.getInt(1));
				reimbursement.setAmount(rs.getDouble(2));
				reimbursement.setSubmittedDate(rs.getTimestamp(3));
				reimbursement.setResolvedDate(rs.getDate(4));
				reimbursement.setDescription(rs.getString(5));
				reimbursement.setAuthor(rs.getInt(6));
				reimbursement.setResolver(rs.getInt(7));
				reimbursement.setStatusId(rs.getInt(8));
				reimbursement.setTypeId(rs.getInt(9));
				reims.add(reimbursement);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reims;
	}

	// this will return the list of all reimbursement for the particular employees
	@Override
	public List<Reimbursement> getByEmployeeId(int empId) {
		List<Reimbursement> reimsEmp = new ArrayList<Reimbursement>();
		PreparedStatement prepStmt;
		try (Connection con = dbcon.getMyConnection();) {
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_author = ? ORDER BY reimb_id";
			prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, empId);
			ResultSet rs = prepStmt.executeQuery();

			while (rs.next()) {
				Reimbursement reimbursement = new Reimbursement();
				reimbursement.setId(rs.getInt(1));
				reimbursement.setAmount(rs.getDouble(2));
				reimbursement.setSubmittedDate(rs.getTimestamp(3));
				reimbursement.setResolvedDate(rs.getDate(4));
				reimbursement.setDescription(rs.getString(5));
				reimbursement.setAuthor(rs.getInt(6));
				reimbursement.setResolver(rs.getInt(7));
				reimbursement.setStatusId(rs.getInt(8));
				reimbursement.setTypeId(rs.getInt(9));
				reimsEmp.add(reimbursement);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reimsEmp;
	}

	// this will update the approved date, approval name, and status change of the reimbursement by reimId.
	@Override
	public int updateReimbursement(Reimbursement reimbursement) {
		PreparedStatement stmt = null;
		try (Connection con = dbcon.getMyConnection()) {
			String sql = "UPDATE ers_reimbursement SET reimb_resolved = ?, reimb_status_id = ?, reimb_resolver = ? WHERE reimb_id = ?";
			stmt = con.prepareStatement(sql);

			stmt.setDate(1, new Date(reimbursement.getResolvedDate().getTime()));
			stmt.setInt(2, reimbursement.getStatusId());
			stmt.setInt(3, reimbursement.getResolver());
			stmt.setInt(4, reimbursement.getId());

			int changed = stmt.executeUpdate();
			if (changed == 1) {
				return 1; // If updated a record

			} else {
				return -1; // if not updated a record
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	@Override
	public Reimbursement deleteById(int reimId) {
		Reimbursement reimb = null;
		PreparedStatement stmt = null;
		try (Connection con = dbcon.getMyConnection()) {
			String sql = "DELETE FROM ers_reimbursement where reimb_id = ?";
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, reimId);

			ResultSet rs = stmt.executeQuery();
			int result = stmt.executeUpdate();
			while (rs.next()) {
				reimb = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getDate(4),
						rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reimb;
	}

	@Override
	public List<Reimbursement> SelectByStatusType(int statusTypeId) {
		List<Reimbursement> reimsEmp = new ArrayList<Reimbursement>();
		PreparedStatement prepStmt;
		try (Connection con = dbcon.getMyConnection()) {
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_status_id = ? ORDER BY reimb_id";
			prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, statusTypeId);
			ResultSet rs = prepStmt.executeQuery();

			while (rs.next()) {
				Reimbursement reimbursement = new Reimbursement();
				reimbursement.setId(rs.getInt(1));
				reimbursement.setAmount(rs.getDouble(2));
				reimbursement.setSubmittedDate(rs.getTimestamp(3));
				reimbursement.setResolvedDate(rs.getDate(4));
				reimbursement.setDescription(rs.getString(5));
				reimbursement.setAuthor(rs.getInt(6));
				reimbursement.setResolver(rs.getInt(7));
				reimbursement.setStatusId(rs.getInt(8));
				reimbursement.setTypeId(rs.getInt(9));
				reimsEmp.add(reimbursement);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reimsEmp;
	}
}
