package com.example.dao;

import java.util.Set;

import com.example.model.Employee;


public interface GenericDao<E> {
	
	
	//Registers an employee in the database
	public boolean insert(Employee employee);
	
	// Update information for specific employee in the database 
	public boolean update(Employee employee);
	
	// Getting information of specific employee based on employee id;
	public Employee selectById(int empId);
	
	// getting information of speficic employee based on username 
	public Employee selectByUser(String username);
	
	// returning information of all employees 
	public Set<Employee> selectAll();
	
	// delete employee users information form the database.
	
	public boolean deleteEmployeeUser(int empId);

	
}
