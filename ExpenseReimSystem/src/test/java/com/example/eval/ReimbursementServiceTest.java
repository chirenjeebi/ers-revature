package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ReimbursementDao;
import com.example.model.Reimbursement;
import com.example.service.ReimbursementService;


public class ReimbursementServiceTest {
	
	@Mock
	private ReimbursementDao mockedDao;
	private ReimbursementService testService = new ReimbursementService(mockedDao);
	private Reimbursement testReim; 
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		
	}
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		testService = new ReimbursementService(mockedDao);
		testReim = new Reimbursement(1, 100, new Timestamp(0), new Date(), "travelling", 1, 5, 3, 2);
		when(mockedDao.selectRequestAll()).thenReturn((List) testReim);

	}
	
	@After
	public void tearDown() throws Exception{
		
	}
	
	@Test
	public void testGetReimbusermentRequest() {
		assertEquals(testService.getReimburmentList(), testReim.getId());
	}

}
