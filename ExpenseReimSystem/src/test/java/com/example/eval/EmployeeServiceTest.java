package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.EmployeeDao;
import com.example.model.Employee;
import com.example.model.UserRoles;
import com.example.service.EmployeeService;

public class EmployeeServiceTest {
	
	@Mock
	private EmployeeDao mockedDao;
	private EmployeeService testService = new EmployeeService(mockedDao);
	private Employee testEmployee;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		
	}
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		testService = new EmployeeService(mockedDao);
		testEmployee = new Employee(1, "User1", "Test" , "user1", "password", "user1@gmail.com",2);
		when(mockedDao.selectByUser("user1")).thenReturn(testEmployee);
	}
	
	@After
	public void tearDown() throws Exception{
		
	}
	
	@Test
	public void testGetUserSuccess() {
		assertEquals(testService.getUser("user1").getUsername(), testEmployee.getUsername());
	}
	
	@Test(expected = NullPointerException.class)
	public void testGetUserFailure() {
		assertEquals(testService.getUser("abcdefg"), null);
	}
	
	@Test
	public void testVerifyPasswordSuccess() {
		assertTrue(testService.userVerify("user1", "password"));
	}
	
	@Test
	public void testVerifyPasswordFailure() {
		assertFalse(testService.userVerify("user1", "user"));
	}
	
}
