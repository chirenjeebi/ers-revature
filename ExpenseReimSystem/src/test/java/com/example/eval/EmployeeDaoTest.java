package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.connection.DBConnection;
import com.example.dao.EmployeeDao;
import com.example.model.Employee;
import com.example.model.UserRoles;

public class EmployeeDaoTest {
	private static String url = "jdbc:mariadb://database-1.crm2rxoxsody.us-east-1.rds.amazonaws.com:3306/projectdb";
	private static String username = "projectuser";
	private static String password = "mypassword";
	
	@Mock
	private DBConnection dbCon;
	
	@Mock
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	private Employee testEmployee;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		
	}
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		when(dbCon.getMyConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		testEmployee = new Employee(1, "User1", "Test" , "user1", "password", "user1@gmail.com", 2);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testEmployee.getId());
		when(rs.getString(2)).thenReturn(testEmployee.getFirstName());
		when(rs.getString(3)).thenReturn(testEmployee.getLastName());
		when(rs.getString(4)).thenReturn(testEmployee.getUsername());
		when(rs.getString(5)).thenReturn(testEmployee.getPassword());
		when(rs.getString(6)).thenReturn(testEmployee.getEmail());
		when(rs.getInt(7)).thenReturn(testEmployee.getUserRole());
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.executeUpdate()).thenReturn(1);
	}
	
	@After
	public void tearDown() throws Exception{
		
	}
	
	@Test
	public void testFindByUserSuccess() {
		assertEquals(new EmployeeDao(dbCon).selectByUser("user1").getUsername(), testEmployee.getUsername());
	}

}
